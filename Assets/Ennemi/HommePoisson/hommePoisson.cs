﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class hommePoisson : MonoBehaviour
{
    public float speed;
    private int direction = 1;
    private Rigidbody2D rb;

    public Transform left, right;
    // Start is called before the first frame update
    void Start()
    {
        rb = GetComponent<Rigidbody2D>();
    }

    // Update is called once per frame
    void Update()
    {
       rb.velocity = new Vector2(direction,0);

       if (transform.position.x == left.position.x)
       {
           Turn();
       }

       if (transform.position.x == right.position.x)
       {
           Turn();
       }
    }

    private void OnCollisionEnter2D(Collision2D other)
    {
        if (other.gameObject.tag == "changeDirection")
        {
            Turn();
        }
    }

    public void Turn()
    {
        direction *= -1;
        transform.Rotate(0,180,0);
    }
}

