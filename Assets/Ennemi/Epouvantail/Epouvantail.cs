﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Epouvantail : MonoBehaviour
{
    public Animator animator;

    public bool isEpouvantailAgressiv;

    public int life;
    public int damage;

    public GameObject hero;
    public LayerMask layerHero;

    private float distanceCast = 2f;
    private Vector2 originEpouvantail;
    private Vector2 directionEpouvantail;

    // Start is called before the first frame update
    void Start()
    {
        animator.enabled = false;
        hero = GameObject.FindGameObjectWithTag("Player");
    }
    
    void Update()
    {
    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.gameObject.tag.Equals("Player"))
        {
            if (isEpouvantailAgressiv)
            {
                animator.enabled = true;
                other.GetComponent<MoovDisplay>().TakeDamage(gameObject, damage);
            }
        }
    }
}