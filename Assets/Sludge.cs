﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Sludge : MonoBehaviour
{
    public int damageSludge = 3;
    public float speed = 4f;
    public Rigidbody2D rb2D;
    public GameObject GrosseLimace;

    // Start is called before the first frame update
    void Start()
    {
        if (GetComponent<GrosseLimace>().direction > 0)
        { 
            rb2D.velocity = transform.right * speed;
        }
        else if (GetComponent<GrosseLimace>().direction < 0)
        {
            rb2D.velocity = -transform.right * speed;
        }
        
    }
    
    public void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "Player")
        {
            collision.GetComponent<MoovDisplay>().TakeDamage(gameObject, damageSludge);
            Destroy(gameObject);
        }
    }

    // Update is called once per frame
    void Update()
    {
    }
}