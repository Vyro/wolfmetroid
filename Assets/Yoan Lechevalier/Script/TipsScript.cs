﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TipsScript : MonoBehaviour
{
    public GameObject gears;
    
    public LayerMask playerLayer;
    public bool playerInRange;
    public bool textShowed;

    public GameObject showTextToThePlayer;
    public GameObject showTextToThePlayer2;
    public GameObject textInstantiated;
    public Transform showTextHere;
    
    void Start()
    {
        gears = GameObject.FindGameObjectWithTag("GameController");
    }
    
    void Update()
    {
        playerInRange = Physics2D.OverlapCircle(transform.position, 3f, playerLayer);
        
        if (playerInRange)
        {
            ShowText();
            
            Vector2 textPos = Camera.main.WorldToScreenPoint(showTextHere.position);
            textInstantiated.transform.position = textPos;
        }
        else
        {
            Destroy(textInstantiated);
            textShowed = false;
        }
    }
    
    public void ShowText()
    {
        if (!textShowed)
        {
            if (gears.GetComponent<GearsScript>().player.GetComponent<Bonus2CasseBrike>() == null)
            {
                GameObject textInstantiated2 = Instantiate(showTextToThePlayer);
                textInstantiated = textInstantiated2;
                textInstantiated.transform.SetParent(gears.GetComponent<GearsScript>().canvasMain.transform);
            }
            else
            {
                GameObject textInstantiated2 = Instantiate(showTextToThePlayer2);
                textInstantiated = textInstantiated2;
                textInstantiated.transform.SetParent(gears.GetComponent<GearsScript>().canvasMain.transform);
            }
        }
        textShowed = true;
    }

}
