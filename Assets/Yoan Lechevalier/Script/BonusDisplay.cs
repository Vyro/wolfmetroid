﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEditorInternal;
using UnityEngine;

public class BonusDisplay : MonoBehaviour
{
    public BonusScriptable me;
    public SpriteRenderer mySprite;

    void Start()
    {
        if (gameObject.TryGetComponent(out SpriteRenderer spriteRenderer))
        {
            mySprite = spriteRenderer;
        }

        mySprite.sprite = me.bonusSprite;
    }

    void Update()
    {
      
    }

    public void OnCollisionEnter2D(Collision2D col)
    {
        if (col.gameObject.tag == "Player")
        {
            //col.gameObject.GetComponent<MoovDisplay>().GetBonus(bonusScript);

            if (transform.GetChild(0).TryGetComponent(out MonoBehaviour monoBehaviour)) //toujours mettre le gameobject empty en premier
            {
                col.gameObject.AddComponent(monoBehaviour.GetType());
            }
            
            Destroy(gameObject);
        }
    }
}
