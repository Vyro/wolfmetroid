﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "New Bonus", menuName = "Bonus")]
public class BonusScriptable : ScriptableObject
{
  public new string name;

  public Sprite bonusSprite;

  public MonoBehaviour bonusScript;
}
