﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Paralax : MonoBehaviour
{
    public float length, startpos;
    public float lengthY;
    public Vector2 startPos;
    public GameObject gears;
    public GameObject cam;
    public float parallaxEffect;
    
    void Start()
    {
        gears = GameObject.FindGameObjectWithTag("GameController");

        startPos = transform.position;
        startpos = transform.position.x;
        length = transform.localScale.x;
        lengthY = transform.localScale.y;
    }
    
    void Update()
    {
        cam = gears.GetComponent<GearsScript>().cam.gameObject;
        
        float dist = (cam.transform.position.x * parallaxEffect);
        float temp = (cam.transform.position.x * (1 - parallaxEffect));
        
        float distY = (cam.transform.position.y * parallaxEffect);
        float tempY =  (cam.transform.position.y * (1 - parallaxEffect));

        float test = cam.transform.position.y / 5f;
        
        transform.position = new Vector3(startpos + dist, startPos.y + distY - test, transform.position.z);//startPos.y + distY

        if (temp > startpos + length)
        {
            startpos += length;
        }
        else if(temp < startpos - length)
        {
            startpos -= length;
        }

        if (tempY > startPos.y + lengthY)
        {
            startPos = new Vector2(startPos.x, startPos.y + lengthY);
        }
        else if(temp < startPos.y - lengthY)
        {
            startPos = new Vector2(startPos.x, startPos.y - lengthY);
        }
    }
}
