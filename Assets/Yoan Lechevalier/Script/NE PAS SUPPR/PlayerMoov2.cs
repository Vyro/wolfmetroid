﻿using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using UnityEngine;

public class PlayerMoov2 : MonoBehaviour
{
    public Rigidbody2D rigi;

    public float formTSpeed;

    public float jumpForce2;

    public float jumpForceWall2;

    public float wallSpeed2;

    public float acceleration;

    public int damageFormT;

    public float attackRangeFormF;

    public float stickness;
    public float stickNessChange;
    public float stickNess2;

    public bool canAttackFormT = true;
    public GameObject attackFeedBack;
    
    void Start()
    {
        rigi = transform.root.gameObject.GetComponent<Rigidbody2D>();
        jumpForceWall2 = 15;
        canAttackFormT = true;
    }
    
    void Update()
    {
        
    }

    public void Moov2()
    {
        float direction = Input.GetAxis("Horizontal");

        if (direction != 0)
        {
            if (formTSpeed < 0.1f)
            {
                formTSpeed += Time.deltaTime * acceleration;
            }
        }
        else
        {
            formTSpeed = 0;
        }

        if (transform.root.gameObject.GetComponent<MoovDisplay>().grounded && !transform.root.GetComponent<MoovDisplay>().inTheAir || !transform.root.gameObject.GetComponent<MoovDisplay>().onWallLeft && !transform.root.gameObject.GetComponent<MoovDisplay>().onWallRight)
        {
            transform.root.gameObject.transform.Translate(new Vector3(formTSpeed * direction * Time.timeScale,0,0));
        }else if (transform.root.gameObject.GetComponent<MoovDisplay>().onWallLeft && (Input.GetButton("Right") || Input.GetButton("D")))
        {
            //float stickNess2 = 0;

            if (stickNess2 < 0.1f)
            {
                stickNess2 += Time.deltaTime * stickNessChange;
            }

            if (stickNess2 >= 0.099f)
            {
                stickness = 1;
            }else
            {
                stickness = 0;
            }
            
            transform.root.gameObject.transform.Translate(new Vector3(formTSpeed * 1 * stickness,0,0));
        } else if(transform.root.gameObject.GetComponent<MoovDisplay>().onWallRight && (Input.GetButton("Left") || Input.GetButton("Q")))
        {
            //float stickNess2 = 0;

            if (stickNess2 < 0.1f)
            {
                stickNess2 += Time.deltaTime * stickNessChange;
            }

            if (stickNess2 >= 0.099f)
            {
                stickness = 1;
            }
            else
            {
                stickness = 0;
            }
            
            transform.root.gameObject.transform.Translate(new Vector3(formTSpeed * -1 * stickness,0,0));
        }

        if (stickness == 1 && !transform.root.gameObject.GetComponent<MoovDisplay>().onWallLeft && !transform.root.gameObject.GetComponent<MoovDisplay>().onWallRight)
        {
            stickNess2 = 0;
        }
         
        if (transform.root.gameObject.GetComponent<MoovDisplay>().grounded && !transform.root.GetComponent<MoovDisplay>().inTheAir)
        {
            rigi.velocity = new Vector2(0, rigi.velocity.y);
        }
    }
    
    public void Jump2()
    {
        if (transform.parent.gameObject.GetComponent<MoovDisplay>().grounded && !transform.root.GetComponent<MoovDisplay>().inTheAir ||  !transform.root.GetComponent<MoovDisplay>().inTheAir)
        {
            rigi.velocity = new Vector2(rigi.velocity.x, jumpForce2);
        }
       /* else if (transform.parent.gameObject.GetComponent<MoovDisplay>().onWallLeft)
        {
            rigi.AddForce(new Vector2(25,30), ForceMode2D.Impulse);
        }
        else if (transform.parent.gameObject.GetComponent<MoovDisplay>().onWallRight)
        {
            rigi.AddForce(new Vector2(-25,30 ), ForceMode2D.Impulse);
        }*/
        else if (transform.parent.gameObject.GetComponent<MoovDisplay>().bonusJump > 0 && !transform.parent.gameObject.GetComponent<MoovDisplay>().onWallLeft && !transform.parent.gameObject.GetComponent<MoovDisplay>().onWallRight)
        {
            rigi.velocity = Vector2.up * jumpForce2;
            transform.parent.gameObject.GetComponent<MoovDisplay>().bonusJump--;
            Instantiate(attackFeedBack, transform.root.position + new Vector3(0, -0.5f, 0), attackFeedBack.transform.rotation);
        }
    }
    
    public void SlideWall2()
    {
        if (rigi.velocity.y < -wallSpeed2 &&  transform.parent.gameObject.GetComponent<MoovDisplay>().onWallLeft || rigi.velocity.y < -wallSpeed2 &&  transform.parent.gameObject.GetComponent<MoovDisplay>().onWallRight) 
        {
            rigi.velocity = new Vector2(rigi.velocity.x, -wallSpeed2);
        }
    }
    
    public void AttackPlayerT()
    {
        if (canAttackFormT)
        {
            Collider2D[] hit = Physics2D.OverlapCircleAll(transform.parent.gameObject.GetComponent<MoovDisplay>().attackPos.position, attackRangeFormF, transform.parent.gameObject.GetComponent<MoovDisplay>().gears.GetComponent<GearsScript>().ennemiLayer);

           /*if (transform.root.gameObject.GetComponent<MoovDisplay>().facingLeft)
           {
               var instantiaedTruc = Instantiate(attackFeedBack, transform.root.gameObject.GetComponent<MoovDisplay>().attackPos.position + new Vector3(-0.5f, 0,0),
                   attackFeedBack.transform.rotation);
           }
           else
           {
               var instantiaedTruc = Instantiate(attackFeedBack, transform.root.gameObject.GetComponent<MoovDisplay>().attackPos.position + new Vector3(0.5f, 0,0),
                   attackFeedBack.transform.rotation);
           }*/

           foreach (var go in hit)
            {
                if (go.TryGetComponent(out ennemiTakeDamage _ennemiTakeDamage))
                {
                    _ennemiTakeDamage.TakeDmage(transform.root.gameObject, damageFormT);
                }
            }

            canAttackFormT = false;
            transform.root.GetComponent<SpriteRenderer>().DOColor(transform.root.GetComponent<SpriteRenderer>().color, 0.3f).OnComplete(CanAttackAgainFormT);
        }
    }

    public void CanAttackAgainFormT()
    {
        canAttackFormT = true;
    }
}
