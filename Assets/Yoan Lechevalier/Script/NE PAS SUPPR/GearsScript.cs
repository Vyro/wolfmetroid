﻿using System;
using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using UnityEditorInternal;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.Tilemaps;
using UnityEngine.UI;

public class GearsScript : MonoBehaviour
{
    public string sceneToLoadName;
    
    public Camera cam;
    public GameObject player;
    public Canvas canvasMain;
    
    public LayerMask platform;
    public LayerMask ennemiLayer;
    
    public Tilemap tileMapMain;
    public Tilemap tileMapDestr;
    
    public GameObject Clock;
    public GameObject hpBar;
    public GameObject gameOverPanel;
    public GameObject uiPlayerPanel;
    public GameObject menuPanel;
    public GameObject pausePanel;
    public bool gamePaused;

    public GameObject nightPanel;
    public GameObject sun;
    public float gameTimeMulti = 10f; //tous multiplier pour mettre les heures de l'univers du jeu en minutes IRL(pour le système jour/nuit)

    public float dayTime;
    public List<BonusOnThePlayer> bonusesPlayerHave = new List<BonusOnThePlayer>();

    public bool test;

    void Start()
    {
        if (!test)
        {
            SceneManager.LoadScene("MenuScene");
        }
    }


    void Update()
    {
        if (Input.GetButtonDown("Cancel"))
        {
            Pause();
        }
        
        if (GameObject.FindGameObjectWithTag("Player") != null)
        {
            player = GameObject.FindGameObjectWithTag("Player");
        }
        
        cam = Camera.main;
        
        if (GameObject.FindGameObjectWithTag("TileMapMain").TryGetComponent(out Tilemap tile))
        {
            tileMapMain = tile;
        }

        if (GameObject.FindGameObjectWithTag("TileMapDestr") != null)
        {
            if (GameObject.FindGameObjectWithTag("TileMapDestr").TryGetComponent(out Tilemap tilemap))
            {
                tileMapDestr = tilemap;
            }
        }

        if (GameObject.FindGameObjectWithTag("Sun") != null)
        {
            sun = GameObject.FindGameObjectWithTag("Sun");
        }
        
        dayTime += Time.deltaTime;//* gameTimeMulti;
        
        if (dayTime < 7 || dayTime > 21)
        {
           tileMapMain.color = Color.gray;
           nightPanel.GetComponent<Image>().DOFade(0.5f, 4);

           if (sun.TryGetComponent(out SpriteRenderer spriteRenderer))
           {
               //sun.GetComponent<Image>().color = Color.white;
               spriteRenderer.color = Color.white;
           }

           if (player.TryGetComponent(out MoovDisplay test))
            {
                test.form = true;
            }
        }
        else
        {
            tileMapMain.color = Color.white;
            nightPanel.GetComponent<Image>().DOFade(0, 4);
            
            if (sun.TryGetComponent(out SpriteRenderer spriteRenderer))
            {
                //sun.GetComponent<Image>().color = new Color(255, 182, 69, 225);
                spriteRenderer.color = Color.yellow;
            }

            if (player.TryGetComponent(out MoovDisplay test))
            {
                test.form = false;
            }
        }

        if (dayTime >= 24)
        {
            dayTime = 0;
        }

        Clock.transform.eulerAngles = new Vector3(0,0,-dayTime * 30);  //360 / 24 = 15 * 2(la moitier de l'horloge) = 30
        
        if (player.TryGetComponent(out MoovDisplay moovDisplay))
        {
            hpBar.transform.localScale = new Vector3(moovDisplay.hp / moovDisplay.maxHp, hpBar.transform.localScale.y, 0);
        }

        if (player.GetComponent<MoovDisplay>().hp <= 0)
        {
            GameOver();
        }
    }
    
    public void LoadGameScene()
    {
        SceneManager.LoadScene(sceneToLoadName); //mettre la scene du premier niveau;TestY, ZoneChamps
        uiPlayerPanel.SetActive(true);
        menuPanel.SetActive(false);
        gameOverPanel.SetActive(false);
    }

    public void LoadMenu()
    {
        SceneManager.LoadScene("MenuScene");
        gameOverPanel.SetActive(false);
        menuPanel.SetActive(true);
    }

    public void Pause()
    {
        if (!gamePaused)
        {
            pausePanel.SetActive(true);
            gamePaused = true;
            Time.timeScale = 0;
        }
        else
        {
            pausePanel.SetActive(false);
            gamePaused = false;
            Time.timeScale = 1;
        }
    }

    public void Quit()
    {
        Application.Quit();
    }

    public void GameOver()
    {
        gameOverPanel.SetActive(true);
        player.GetComponent<MoovDisplay>().GameOverPlayer();
    }

    public void GiveAllTheBonusToThePlayer() //changer de scène EN détruisant je joueur: à appeler à chaque fois que ont charge une scène si ont détruit le joueur
    {
        foreach (var bonus in bonusesPlayerHave)
        {
            player.AddComponent(bonus.myScript.GetType());
        }
    }

    public void ChangeScene(string nameOfTheScene, GameObject door) // pour changer de scène SANS détruire le joueur
    {
        SceneManager.LoadScene(nameOfTheScene);
        player.transform.position = door.GetComponent<ChangeZone>().whereToSpawnThePlayerNext;
    }
}

public class BonusOnThePlayer
{
    public new string name;
    public bool playerGotIt;
    public Type myType;
    public MonoBehaviour myScript;
}