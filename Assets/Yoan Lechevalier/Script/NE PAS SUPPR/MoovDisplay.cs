﻿using System;
using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using UnityEngine;
using UnityEngine.Tilemaps;

public class MoovDisplay : MonoBehaviour
{
   public GameObject gears;

    public GameObject formT;
    public GameObject formF;
    public SpriteRenderer playerSprite;
    public Sprite formTSprite;
    public Sprite formFSprite;
    public Color baseColor;
    public Rigidbody2D rigi;
    public Transform attackPos;

    public bool form;
    public bool onWallLeft;
    public bool onWallRight;
    public bool facingLeft;
    public bool grounded;
    public bool inTheAir;
    public float distanceGround;

    public Vector3 baseScale;

    public int bonusJump;
    public int baseJump;

    public float maxHp;
    public float hp;

    public Transform feets;

    public Tile test;


    void Start()
    {
        rigi = GetComponent<Rigidbody2D>();
        gears = GameObject.FindGameObjectWithTag("GameController");
        playerSprite = GetComponent<SpriteRenderer>();
        baseColor = playerSprite.color;
        baseScale = transform.localScale;
        hp = maxHp;
    }
    
    void Update()
    {
        float direction;
        direction = Input.GetAxis("Horizontal");

        if (distanceGround >= 1)
        {
            inTheAir = true;
        }
        else
        {
            inTheAir = false;
        }
        
        if (form)
        {
            formT.GetComponent<PlayerMoov2>().Moov2();
            
            formT.GetComponent<PlayerMoov2>().SlideWall2();
            rigi.mass = 3.5f;
            playerSprite.sprite = formTSprite;

            if (Input.GetButtonDown("Fire1")) //attacker
            {
                formT.GetComponent<PlayerMoov2>().AttackPlayerT();
                print("attackFT");
            }
        }
        else
        {
            formF.GetComponent<Player1Moov>().Moov1();
            
            formF.GetComponent<Player1Moov>().SlideWall1();
            rigi.mass = 2;
            playerSprite.sprite = formFSprite;
            
            if (Input.GetButtonDown("Fire1")) //attacker
            {
                formF.GetComponent<Player1Moov>().AttackPlayerF();
                print("attackFF");
            }
        }

        if (Input.GetButtonDown("Jump"))
        {
            if (form)
            {
                formT.GetComponent<PlayerMoov2>().Jump2();
            }
            else
            {
                formF.GetComponent<Player1Moov>().Jump1();
            }
        }

        if (grounded && distanceGround <= 0.5f)
        {
            bonusJump = baseJump;
        }

        if (direction > 0)
        {
            facingLeft = false;
            transform.localScale = new Vector3(baseScale.x, transform.localScale.y, transform.localScale.z);
        }
        if (direction < 0)
        {
            facingLeft = true;
            transform.localScale = new Vector3(-baseScale.x, transform.localScale.y, transform.localScale.z);
        }
        
        grounded = Physics2D.OverlapCircle(feets.position, 0.325f, gears.GetComponent<GearsScript>().platform);
        RaycastHit2D hit = Physics2D.Raycast(feets.position, Vector2.down, Mathf.Infinity, gears.GetComponent<GearsScript>().platform);
        distanceGround = Vector2.Distance(feets.position, hit.point);

        onWallLeft = Physics2D.OverlapCircle(transform.position +  new Vector3(-0.35f,0.2f, 0), 0.4f, gears.GetComponent<GearsScript>().platform);//+ new Vector3(-0.5f,0.2f, 0)
        //onWallLeft = Physics2D.Raycast(transform.position, Vector2.left,3, gears.GetComponent<GearsScript>().platform);
        onWallRight = Physics2D.OverlapCircle(transform.position + new Vector3(0.35f,0.2f, 0), 0.4f, gears.GetComponent<GearsScript>().platform);
        //onWallLeft = Physics2D.Raycast(transform.position, Vector2.right,3, gears.GetComponent<GearsScript>().platform);

        /*if (Input.GetButtonDown("Fire2"))//test
        {
            //gears.GetComponent<GearsScript>().tileMapMain.SetTile(gears.GetComponent<GearsScript>().tileMapMain.WorldToCell(attackPos.position), test); //changer la tete d'une tile
            //rigi.MovePosition(new Vector2(rigi.position.x + 2 * Time.deltaTime, rigi.position.y));

            print(gears.GetComponent<GearsScript>().tileMapMain
                .GetTile(gears.GetComponent<GearsScript>().tileMapMain.WorldToCell(attackPos.position)));
        }*/
    }

    public void TakeDamage(GameObject go, int test)
    {
        if (playerSprite.color == baseColor)
        {
            //rigi.AddForce(go.transform.right * 1000);// changer le "* 1000" par la puisssance, dans un script du gameobject
            playerSprite.DOColor(Color.red, 1).OnComplete(ReturnBaseColor);
            hp -= test;
        }
    }

    public void ReturnBaseColor()
    {
        playerSprite.color = baseColor;
    }
    
    public void GetBonus(MonoBehaviour test)
    {
        /*Type x = typeof(MoovDisplay);
        print(typeof(MoovDisplay));
        gameObject.AddComponent(typeof(PlayerMoov));*/  //le Labo(test)
        gameObject.AddComponent(test.GetType()); 
    }

    public void GameOverPlayer()
    {
        if (gameObject.GetComponent<BonusScript1>() != null)
        {
            gameObject.GetComponent<BonusScript1>().GameOverBonus1();
        }
        
        if (gameObject.GetComponent<Bonus2CasseBrike>() != null)
        {
            gameObject.GetComponent<Bonus2CasseBrike>().GameOverBonus2();
        }
    }
}

