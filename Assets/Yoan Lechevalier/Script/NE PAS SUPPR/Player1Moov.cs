﻿using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using UnityEngine;
using UnityEngine.XR;

public class Player1Moov : MonoBehaviour
{
    public Rigidbody2D rigi;

    public float formFSpeed;

    public float jumpForce1;

    public float jumpForceWall1;

    public float wallSpeed1;

    public Vector2 wallJump;

    public int damageFormF;

    public float AttackRangeFormF;

    public float stickNess;
    public float stickNessChange;
    public float stickNess2;

    public bool canAttackFormF = true;
    public GameObject attackFeedBack;
    
    void Start()
    {
        rigi = transform.root.gameObject.GetComponent<Rigidbody2D>();
        jumpForceWall1 = 10;
        canAttackFormF = true;
    }
    
    void Update()
    {
        if (wallJump.x > 0 && wallJump.y > 0)
        {
            wallJump.x -= Time.deltaTime * 5;
            wallJump.y -= Time.deltaTime * 5;
        }
        else
        {
            wallJump.x += Time.deltaTime * 5;
            wallJump.y += Time.deltaTime * 5;
        }
    }

    public void Moov1()
    {
        float direction = Input.GetAxis("Horizontal");

        if (transform.root.gameObject.GetComponent<MoovDisplay>().grounded && !transform.root.GetComponent<MoovDisplay>().inTheAir || !transform.root.gameObject.GetComponent<MoovDisplay>().onWallLeft && !transform.root.gameObject.GetComponent<MoovDisplay>().onWallRight)
        {
            transform.root.gameObject.transform.Translate(new Vector3(formFSpeed * direction * Time.timeScale,0,0));
        }else if (transform.root.gameObject.GetComponent<MoovDisplay>().onWallLeft && (Input.GetButton("Right") || Input.GetButton("D")))
        {
            //float stickNess2 = 0;
            
            if (stickNess2 < 0.1f)
            {
                stickNess2 += Time.deltaTime * stickNessChange;
            }

            if (stickNess2 >= 0.099f)
            {
                stickNess = 1;
            }
            else
            {
                stickNess = 0;
            }
            
            transform.root.gameObject.transform.Translate(new Vector3(formFSpeed * 1 * stickNess,0,0));
        } else if(transform.root.gameObject.GetComponent<MoovDisplay>().onWallRight && (Input.GetButton("Left") || Input.GetButton("Q")))
        {
            //float stickNess2 = 0;
            
            if (stickNess2 < 0.1f)
            {
                stickNess2 += Time.deltaTime * stickNessChange;
            }

            if (stickNess2 >= 0.099f)
            {
                stickNess = 1;
            }
            else
            {
                stickNess = 0;
            }
            
            transform.root.gameObject.transform.Translate(new Vector3(formFSpeed * -1 * stickNess,0,0));
        }
        
        if (stickNess == 1 && !transform.root.gameObject.GetComponent<MoovDisplay>().onWallLeft && !transform.root.gameObject.GetComponent<MoovDisplay>().onWallRight)
        {
            stickNess2 = 0;
        }

        if (transform.root.gameObject.GetComponent<MoovDisplay>().grounded && !transform.root.GetComponent<MoovDisplay>().inTheAir)
         {
             rigi.velocity = new Vector2(0, rigi.velocity.y);
         }

         /*if (!transform.parent.gameObject.GetComponent<MoovDisplay>().onWallLeft && transform.parent.gameObject.GetComponent<MoovDisplay>().onWallRight)
         {
             
         }*/
    }

    public void Jump1()
    {
        if (transform.parent.gameObject.GetComponent<MoovDisplay>().grounded && !transform.root.GetComponent<MoovDisplay>().inTheAir ||  !transform.root.GetComponent<MoovDisplay>().inTheAir)  //saut sur le sol
        {
            rigi.velocity = new Vector2(rigi.velocity.x, jumpForce1);
        }
        /*else if (transform.parent.gameObject.GetComponent<MoovDisplay>().onWallLeft)  //saut contre un mur
        {
            rigi.AddForce(new Vector2(15,20), ForceMode2D.Impulse);
        }
        else if (transform.parent.gameObject.GetComponent<MoovDisplay>().onWallRight)
        {
            rigi.AddForce(new Vector2(-15,20), ForceMode2D.Impulse);
        }*/
        else if (transform.parent.gameObject.GetComponent<MoovDisplay>().bonusJump > 0 && !transform.parent.gameObject.GetComponent<MoovDisplay>().onWallLeft && !transform.parent.gameObject.GetComponent<MoovDisplay>().onWallRight)  // saut dans les air
        {
            rigi.velocity = Vector2.up * jumpForce1;
            transform.parent.gameObject.GetComponent<MoovDisplay>().bonusJump--;
            Instantiate(attackFeedBack, transform.root.position + new Vector3(0, -0.5f, 0), attackFeedBack.transform.rotation);
        }
        
    }

    public void SlideWall1()
    {
        if (rigi.velocity.y < -wallSpeed1 &&  transform.parent.gameObject.GetComponent<MoovDisplay>().onWallLeft || rigi.velocity.y < -wallSpeed1 &&  transform.parent.gameObject.GetComponent<MoovDisplay>().onWallRight) 
        {
            rigi.velocity = new Vector2(rigi.velocity.x, -wallSpeed1);
        }
    }

    public void AttackPlayerF()
    {
        if (canAttackFormF)
        {
            Collider2D[] hit = Physics2D.OverlapCircleAll(transform.parent.gameObject.GetComponent<MoovDisplay>().attackPos.position, AttackRangeFormF, transform.parent.gameObject.GetComponent<MoovDisplay>().gears.GetComponent<GearsScript>().ennemiLayer);
            
            /*if (transform.root.gameObject.GetComponent<MoovDisplay>().facingLeft)
            {
                var instantiaedTruc = Instantiate(attackFeedBack, transform.root.gameObject.GetComponent<MoovDisplay>().attackPos.position + new Vector3(-0.5f, 0,0), // feedback
                    attackFeedBack.transform.rotation);
            }
            else
            {
                var instantiaedTruc = Instantiate(attackFeedBack, transform.root.gameObject.GetComponent<MoovDisplay>().attackPos.position + new Vector3(0.5f, 0,0),
                    attackFeedBack.transform.rotation);
            }*/
            
            foreach (var go in hit)
            {
                if (go.TryGetComponent(out ennemiTakeDamage _ennemiTakeDamage))
                {
                    _ennemiTakeDamage.TakeDmage(transform.root.gameObject, damageFormF);
                }
            }
        }

        canAttackFormF = false;
        transform.root.GetComponent<SpriteRenderer>().DOColor(transform.root.GetComponent<SpriteRenderer>().color, 0.3f).OnComplete(CanAttackAgainFormF); //just pour mete un timer, NE PAS METTRE transform.DOmmoov sur soit(sa casse tous)
    }

    public void CanAttackAgainFormF()
    {
        canAttackFormF = true;
    }
}
