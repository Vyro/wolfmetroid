﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bonus2CasseBrike : MonoBehaviour
{
    public GameObject gears;
    public BonusOnThePlayer mybonus;
    
    void Start()
    {
        gears = GameObject.FindGameObjectWithTag("GameController");
        
        Component[] test = GetComponents(this.GetType());
        if (test.Length > 1)
        {
            Destroy(this);
        }
        
        if (transform.gameObject.GetComponent<MoovDisplay>() != null)
        {
            mybonus.name = this.name;
            mybonus.playerGotIt = true;
            mybonus.myType = GetType();
            mybonus.myScript = this;
            transform.root.GetComponent<MoovDisplay>().gears.GetComponent<GearsScript>().bonusesPlayerHave.Add(mybonus);
        }
    }
    
    void Update()
    {
        if (Input.GetButton("Fire1"))
        {
            Vector3 destructionPos = gears.GetComponent<GearsScript>().player.GetComponent<MoovDisplay>().attackPos.position;
            
            gears.GetComponent<GearsScript>().tileMapDestr.SetTile(gears.GetComponent<GearsScript>().tileMapDestr.WorldToCell(destructionPos), null);
            gears.GetComponent<GearsScript>().tileMapDestr.SetTile(gears.GetComponent<GearsScript>().tileMapDestr.WorldToCell(destructionPos + new Vector3(0.2f,0.5f,0)), null);
            gears.GetComponent<GearsScript>().tileMapDestr.SetTile(gears.GetComponent<GearsScript>().tileMapDestr.WorldToCell(destructionPos + new Vector3(0.2f,-0.5f,0)), null);
            gears.GetComponent<GearsScript>().tileMapDestr.SetTile(gears.GetComponent<GearsScript>().tileMapDestr.WorldToCell(transform.position + new Vector3(0.2f,-0.4f,0)), null);
        }
    }
    
    public void GameOverBonus2()
    {
        Destroy(this);
    }
}
