﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMoov : MonoBehaviour
{
    public LayerMask platformLayer;

    public Rigidbody2D rigi;
    public bool form;

    public float speed;
    public float forceJump;
    public int bonusJump;
    public int nbrJump;

    public float distanceGround;

    public bool jumping;

    public GameObject moov1;
    public GameObject moov2;

    void Start()
    {
        rigi = GetComponent<Rigidbody2D>();
    }
    
    void Update()
    {
        rigi.velocity = new Vector2(Input.GetAxis("Horizontal") * speed, rigi.velocity.y);

        if (Input.GetAxis("Horizontal") < 0)
        {
            transform.localScale = new Vector3(-1,1,1);
        }
        else if (Input.GetAxis("Horizontal") > 0)
        {
            transform.localScale = new Vector3(1,1,1);
        }
        
        if (Input.GetButtonDown("Jump"))                                       //Saut
        {
            if (!jumping)
            {
                rigi.velocity = Vector2.up * forceJump;
            }
            else if (bonusJump > 0)
            {
                rigi.velocity = Vector2.up * forceJump;
                bonusJump--;
            }
        }
        
        RaycastHit2D hit = Physics2D.Raycast(transform.position, Vector2.down, Mathf.Infinity, platformLayer);
        
        if (hit)
        {
            distanceGround = Vector2.Distance(transform.position, hit.point);

            if (distanceGround > 0.5f)
            {
                jumping = true;
            }
            else
            {
                jumping = false;
                bonusJump = nbrJump;
            }
        }
       // bool = Physics2D.OverlapCircle(transform,float, LayerMask)       cercle pour detecter le sol ou un mur
    }
}
