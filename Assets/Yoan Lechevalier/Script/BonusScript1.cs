﻿using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using UnityEngine;
using Component = UnityEngine.Component;

public class BonusScript1 : MonoBehaviour
{
    public Rigidbody2D rigi;
    public BonusOnThePlayer mybonus;

    void Start()
    {
        if (transform.root.gameObject.TryGetComponent(out Rigidbody2D rg))
        {
            rigi = rg;
        }

        Component[] test = GetComponents(this.GetType());  //enlever les script en trop sur le joueur
        if (test.Length > 1)
        {
            Destroy(this);
        }

        if (transform.gameObject.GetComponent<MoovDisplay>() != null)
        {
            mybonus.name = this.name;
            mybonus.playerGotIt = true;
            mybonus.myType = GetType();
            mybonus.myScript = this;
            transform.root.GetComponent<MoovDisplay>().gears.GetComponent<GearsScript>().bonusesPlayerHave.Add(mybonus);
        }
    }
    
    void Update()
    {
        if (transform.gameObject.GetComponent<MoovDisplay>() != null)
        { 
            if (Input.GetButtonDown("Jump"))
            {
                if (transform.gameObject.GetComponent<MoovDisplay>().form)
                {
                    if (transform.gameObject.GetComponent<MoovDisplay>().onWallLeft &&
                        gameObject.GetComponent<MoovDisplay>().inTheAir)
                    {
                        rigi.AddForce(new Vector2(25, 30), ForceMode2D.Impulse);
                    }

                    if (transform.gameObject.GetComponent<MoovDisplay>().onWallRight &&
                        gameObject.GetComponent<MoovDisplay>().inTheAir)
                    {
                        rigi.AddForce(new Vector2(-25, 30), ForceMode2D.Impulse);
                    }
                }
                else
                {
                    if (transform.gameObject.GetComponent<MoovDisplay>().onWallLeft &&
                        gameObject.GetComponent<MoovDisplay>().inTheAir)
                    {
                            rigi.AddForce(new Vector2(15, 20), ForceMode2D.Impulse);
                    }

                    if (transform.gameObject.GetComponent<MoovDisplay>().onWallRight &&
                        gameObject.GetComponent<MoovDisplay>().inTheAir)
                    {
                            rigi.AddForce(new Vector2(-15, 20), ForceMode2D.Impulse);
                    }
                }
            }
        }
        
    }

    public void GameOverBonus1()
    {
        Destroy(this);
    }
}
