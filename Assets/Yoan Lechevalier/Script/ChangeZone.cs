﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ChangeZone : MonoBehaviour
{
    public GameObject gears;
    public string nameOfTheSceneToLoad;
    public Vector2 whereToSpawnThePlayerNext;
    
    void Start()
    {
        gears = GameObject.FindGameObjectWithTag("GameController");
    }
    
    void Update()
    {
        
    }
    
    public void OnCollisionEnter2D(Collision2D col)
    {
        if (col.gameObject.tag == "Player" && nameOfTheSceneToLoad != null)
        {
           gears.GetComponent<GearsScript>().ChangeScene(nameOfTheSceneToLoad, gameObject);
        }
    }
}
