﻿using System.Collections;
using System.Collections.Generic;
using System.Security.Cryptography;
using DG.Tweening;
using UnityEngine;

public class ennemiTakeDamage : MonoBehaviour
{
    public int hpEnnemi;
    public Rigidbody2D rigi;
    public SpriteRenderer mySprite;
    public Color baseColor;
    
    void Start()
    {
        if (gameObject.TryGetComponent(out Rigidbody2D myRigi))
        {
            rigi = myRigi;
        }

        if (gameObject.TryGetComponent(out SpriteRenderer spriteRenderer))
        {
            mySprite = spriteRenderer;
            baseColor = spriteRenderer.color;
        }
    }
    
    void Update()
    {
        if (hpEnnemi <= 0)
        {
            Destroy(gameObject);
        }
    }

    public void TakeDmage(GameObject go, int damage)
    {
        if (rigi != null)
        {
            if (go.TryGetComponent(out MoovDisplay moovDisplay))
            {
                if (moovDisplay.facingLeft)
                {
                    //rigi.AddForce(-go.transform.right * 100);//remplacer * 10 par push force dans les script loup garou humain?
                    //transform.position = Vector3.MoveTowards(transform.position, new Vector3(transform.position.x - 100, transform.position.y, transform.position.z), Time.deltaTime * 5f);
                    transform.DOMove(new Vector3(transform.position.x - 1, transform.position.y, transform.position.z), 1);
                }
                else
                {
                   // rigi.AddForce(go.transform.right * 100);
                    //transform.position = Vector3.MoveTowards(transform.position, new Vector3(transform.position.x + 100, transform.position.y, transform.position.z), Time.deltaTime * 5f);
                    transform.DOMove(new Vector3(transform.position.x + 1, transform.position.y, transform.position.z), 1);
                }
            }
        }
        mySprite.DOColor(Color.red, 1).OnComplete(ReturnBaseColor);

        hpEnnemi -= damage;
    }
    
    public void ReturnBaseColor()
    {
       mySprite.color = baseColor;
    }
}
