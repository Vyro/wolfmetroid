﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GrosseLimace : MonoBehaviour
{
    public int life;
    public int damage;

    private GameObject gears;
    private GameObject hero;
    
    [Range(0, 1)] public float MoveSpeed;
    public LayerMask layerHero;
    private float distanceCast = 5f;
    private Vector2 originGrosseLimace;
    private Vector2 directionGrosseLimace;

    public Transform cracheBoue;
    
    private bool isDetectHero;
    public float direction;
    
    public Transform sludgeTransform;
    public GameObject sludgePrefab;

    // Start is called before the first frame update
    void Start()
    {
        isDetectHero = false;
    }

    // Update is called once per frame
    void Update()
    {
        originGrosseLimace = transform.position;
        directionGrosseLimace = transform.forward;
        direction = Input.GetAxis("Horizontal");

        if (life <= 0)
        {
            Destroy(this);
        }

        DetectionHero();
    }

    void DetectionHero()
    {
        // Dans Inspector, mettre le layer du héro pour layerHero
        RaycastHit2D hits = Physics2D.CircleCast(originGrosseLimace, 10, directionGrosseLimace, distanceCast, layerHero);

        if (hits)
        {
            Debug.Log("La grosse limace détecte le héro !");
            transform.position = Vector3.MoveTowards(transform.position, hero.transform.position, MoveSpeed);
            
            if (direction > 0)
            {
                transform.localScale = new Vector3(transform.localScale.x, transform.localScale.y, transform.localScale.z);
            }
            if (direction < 0)
            {
                transform.localScale = new Vector3(-transform.localScale.x, transform.localScale.y, transform.localScale.z);
            }
            
            ShootThePlayer();
            isDetectHero = true;
        }
        else
        {
            isDetectHero = false;
        }
    }

    void ShootThePlayer()
    {
        Debug.Log("shoot");
        InvokeRepeating("SpawnBullet", 0.1f, 1.0f);
    }
    
    void SpawnBullet()
    {
        Instantiate(sludgePrefab, sludgeTransform.position, sludgeTransform.rotation);
    }
}