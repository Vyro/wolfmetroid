﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UIElements;

public class Coquatrix : MonoBehaviour
{
    public int life;
    public int damage;

    private GameObject gears;
    private GameObject hero;

    public LayerMask layerHero;
    private float distanceCast = 5f;
    private Vector2 originCoquatrix;
    private Vector2 directionCoquatrix;
    private Rigidbody2D rbCoquatrix;
    public Transform feetCoquatrix;
    [Range(0, 500)] public float forceJump;
    [Range(0, 1)] public float MoveSpeed;

    private bool isAlive;
    private bool isDetectHero;
    private bool isGrounded;

    // Start is called before the first frame update
    void Start()
    {
        isAlive = true;
        isDetectHero = false;
        rbCoquatrix = GetComponent<Rigidbody2D>();

        gears = GameObject.FindGameObjectWithTag("GameController");
        hero = GameObject.FindGameObjectWithTag("Player");
    }

    // Update is called once per frame
    void Update()
    {
        originCoquatrix = transform.position;
        directionCoquatrix = transform.forward;
        isGrounded = Physics2D.OverlapCircle(feetCoquatrix.position, 0.3f, gears.GetComponent<GearsScript>().platform);

        if (life <= 0)
        {
            isAlive = false;
            Destroy(this);
        }
        
        if (Input.GetAxis("Horizontal") > 0)
        {
            transform.localScale = new Vector3(-transform.localScale.x, transform.localScale.y, transform.localScale.z);
        }
        if (Input.GetAxis("Horizontal") < 0)
        {
            transform.localScale = new Vector3(transform.localScale.x, transform.localScale.y, transform.localScale.z);
        }

        DetectionHero();
        Jump();
    }

    void DetectionHero()
    {
        // Dans Inspector, mettre le layer du héro pour layerHero
        RaycastHit2D hits = Physics2D.CircleCast(originCoquatrix, 6, directionCoquatrix, distanceCast, layerHero);

        if (hits)
        {
            Debug.Log("Le coquatrix détecte le héro !");
            isDetectHero = true;
            transform.position = Vector3.MoveTowards(transform.position, hero.transform.position, MoveSpeed);
        }
        else
        {
            isDetectHero = false;
        }
    }

    void Jump()
    {
        if (rbCoquatrix != null)
        {
            if (isDetectHero && isGrounded && rbCoquatrix.velocity.y == 0)
            {
                rbCoquatrix.AddForce(Vector2.up * forceJump);
            }
        }
        else
        {
            Debug.Log("Error : Missing RigidBody2D !");
        }
    }

    private void OnCollisionEnter2D(Collision2D other)
    {
        if (other.gameObject.tag.Equals("Player"))
        {
            other.gameObject.GetComponent<MoovDisplay>().TakeDamage(gameObject, damage);
        }
    }
}